from flask import Flask, request, jsonify, render_template
import psycopg2
import os
from psycopg2 import Error
app = Flask(__name__)

DB_HOST = "10.0.20.4"
DB_NAME = "wightloss"
DB_USER = "ofek"
DB_PASSWORD = os.environ['DB_PASSWORD']

def connect_to_database():
    return psycopg2.connect(
        host=DB_HOST,
        database=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD
    )

@app.route('/data/<name>', methods=['GET'])
def retrieve_data(name):
    try:
        conn = connect_to_database()
        if not conn:
            response = {
                'status': 'error',
                'message': 'Database connection error'
            }
            return jsonify(response), 500

        cur = conn.cursor()

        # Execute the SELECT statement to retrieve data based on name
        select_query = "SELECT * FROM weight_data WHERE name = %s"
        cur.execute(select_query, (name,))
        result = cur.fetchone()

        if result:
            # Retrieve the relevant information from the database
            name = result[0]
            age_value = result[1]
            time = result[2]

            response = {
                'status': 'success',
                'name': name,
                'age_value': age_value,
                'time': time
            }
        else:
            response = {
                'status': 'error',
                'message': 'Data not found for the provided name'
            }

        disconnect_from_database(conn, cur)
        return jsonify(response)

    except psycopg2.Error as e:
        response = {
            'status': 'error',
            'message': 'Database error',
            'error_details': str(e)
        }
        disconnect_from_database(conn, cur)
        return jsonify(response), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port="80")